$(function() {
	// ----------------------
	// code for popup
	if ($('.best-works').length) {
		$('.best-works').magnificPopup({
			delegate: '.best-works__in-detail a',
			removalDelay: 500,
			callbacks: {
				beforeOpen: function() {
					this.st.mainClass = this.st.el.attr('data-effect');
				}
			}
		});
	}

	// popup on Winners
	if ($('.js-init-winners-popup').length) {
		$('.js-init-winners-popup').magnificPopup({
			removalDelay: 500,
			callbacks: {
				beforeOpen: function() {
					this.st.mainClass = this.st.el.attr('data-effect');
				},
				open: function() {
					initWinnersSlider();
				},
				close: function() {
					destroyWinnersSlider();
				}
			}
		});
	}

	initScrollable();

	function initScrollable() {
		if ($(".scrollable").length) {
			$(".scrollable").mCustomScrollbar({
				scrollInertia: 150
			});
		}
	}

	// close popup button
	$(document).on('click', function(e) {
		if ($(e.target).hasClass('js-close-popup')) {
			$.magnificPopup.close();
		}
	});
	// ---------------------/

	// ----------------------
	// scroll to anchor
	$('.js-scroll-to-anchor').click(function(e) {
		e.preventDefault();
		var anchor = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(anchor).offset().top
		}, 600);
	})
	// ---------------------/

	// ---------------------/
	// jquery inview
	$('.js-inview--no-conflict').on('inview', function(event, isInView) {
		if (isInView) {
			$(this).addClass('inview');
		}
	});
	// ---------------------/

	// ---------------------/
	// Basket input
	$('[data-only-numbers]').keydown(function(e) {
		// Allow: backspace, delete, tab, escape, enter
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
			// Allow: Ctrl/cmd+A
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+C
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+X
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow Ctrl+R
			(e.keyCode == 82 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$('.js-basket-item-minus').click(function() {
		var input = $(this).siblings('input');
		var val = input.val();
		var currentValue = val.length ? parseInt(val) : 1;
		if (currentValue > 1) {
			var newValue = currentValue - 1;
			input.val(newValue);
		} else {
			input.val(1);
		}
	});
	$('.js-basket-item-plus').click(function() {
		var input = $(this).siblings('input');
		var val = input.val();
		var currentValue = val.length ? parseInt(val) : 0;
		var newValue = currentValue + 1;
		input.val(newValue);
	});
	// ---------------------/

	// ---------------------/
	// Basket: remove item
	$('.js-remove-basket-item').click(function() {
		$(this).parent().remove();
	});
	// ---------------------/

	// ---------------------
	$('[data-buy-popup]').magnificPopup({
		removalDelay: 500,
		callbacks: {
			beforeOpen: function() {
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		}
	});
	// ---------------------/

	// ---------------------
	// poems
	$('.js-load-new-poems').click(function() {
		$(this).addClass('more-poems__btn--loading');

		setTimeout(function() {
			$('.js-show-after-click-on-load').first().slideDown().removeClass('poems__row--hidden js-show-after-click-on-load');
			if (!$('.js-show-after-click-on-load').length) {
				$(this).fadeOut(500);
			}
		}.bind($(this)), 500);

		setTimeout(function() {
			$(this).removeClass('more-poems__btn--loading');
		}.bind($(this)), 1500);
	});
	// ---------------------/

	// ---------------------
	// feedback
	if ($('.js-init-slider').length) {
		$('.js-init-slider').slick({
			arrows: true,
			fade: true,
			dots: true,
			cssEase: 'linear',
			adaptiveHeight: true
		});
	}
	// ---------------------/

	// ---------------------
	// winners slider
	function destroyWinnersSlider() {
		$('.js-init-winners-slider').slick('unslick');
	}

	function initWinnersSlider() {
		var sliders = $('.js-init-winners-slider');

		sliders.each(function() {
			var slider = $(this);
			var prevArrow = slider.siblings('.winners-slider-arrows').find('.winners-slider-arrow--prev');
			var nextArrow = slider.siblings('.winners-slider-arrows').find('.winners-slider-arrow--next');

			slider.slick({
				arrows: true,
				fade: true,
				dots: true,
				cssEase: 'linear',
				adaptiveHeight: true,
				prevArrow: prevArrow,
				nextArrow: nextArrow,
				responsive: [{
					breakpoint: 991,
					settings: {
						dots: false
					}
				}]
			});
			slider.on('afterChange', function() {
				if ($(window).width() < 992) {
					setWinnersSliderArrowsPosition();
				} else {
					unsetWinnersSliderArrowsPosition();
				}
			});
		});

		if ($(window).width() < 992) {
			setWinnersSliderArrowsPosition();
		} else {
			unsetWinnersSliderArrowsPosition();
		}
	}

	function setWinnersSliderArrowsPosition() {
		var top = $('.winners-slider__item.slick-active .best-works-popup__col--left').height() / 2 + parseInt($('.best-works-popup__row').css('padding-top'));
		$('.js-set-position-on-mobile').css('top', top + 'px');

	}

	function unsetWinnersSliderArrowsPosition() {
		$('.js-set-position-on-mobile').removeAttr('style');
	}
	// ---------------------/

	// ---------------------
	// conditions slider
	if ($('.js-init-conditions-slider').length) {
		$('.js-init-conditions-slider').slick({
			arrows: true,
			fade: true,
			dots: true,
			cssEase: 'linear',
			adaptiveHeight: true
		});
	}
	// ---------------------/

	// ---------------------
	// scroll to anchor
	$('.js-scroll-to-anchor').click(function(e) {
		e.preventDefault();

		var href = $(this).attr('href');
		var anchor = $(href);
		if (anchor.length) {
			$('html, body').animate({
				scrollTop: anchor.offset().top
			}, 1000, function() {
				window.location.hash = href;
			});
		}
	})
	// ---------------------/

	// winners navigation
	$('.js-go-to-this-menu-item').click(function() {
		$('.js-go-to-this-menu-item').removeClass('active');
		$(this).addClass('active');

		var tab = $(this).data('tab');
		$('.js-toggle-content').removeClass('active');
		$(tab).addClass('active');
	});

	$('.js-init-winners-styler').styler({
		selectPlaceholder: "ПЕРВОЕ МЕСТО"
	});
	$('select.js-init-winners-styler').on('change', function() {
		var tab = $(this).val();
		$('.js-toggle-content').removeClass('active');
		$(tab).addClass('active');
	});
	$(window).on('resize', function() {
		setActiveWinnersSelect();
		if ($(window).width() < 992) {
			setWinnersSliderArrowsPosition();
		} else {
			unsetWinnersSliderArrowsPosition();
		}
	})

	function setActiveWinnersSelect() {
		var activeTab = $('.js-go-to-this-menu-item.active').data('tab');
		$('select.js-init-winners-styler').val(activeTab).change();

	}
	// $('select.js-init-winners-styler').on('change', function(){
	//     var val = $(this).val();
	//     $('.js-go-to-this-menu-item').removeClass('active');
	//     $('.js-go-to-this-menu-item[data-tab=' + val + ']').addClass('active').trigger('click');
	// })
	// winners navigation --/

});
// contest button
$(function() {
	$('.js-toggle-contest-dropdown').click(function() {
		$('.contest').toggleClass('contest--open');
	});
});
// end of contest button
// timer
$(function() {
	$('[data-countdown]').each(function() {
		var $this = $(this),
			finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
			var D = event.strftime('%D').split('');
			var H = event.strftime('%H').split('');
			var M = event.strftime('%M').split('');
			var S = event.strftime('%S').split('');

			var Dhtml = '<div class="timer__element"><div class="timer__values"><span class="timer__digit">' + D[0] + '</span><span class="timer__digit">' + D[1] + '</span></div><div class="timer__caption">Дни</div></div>';
			var Hhtml = '<div class="timer__element"><div class="timer__values"><span class="timer__digit">' + H[0] + '</span><span class="timer__digit">' + H[1] + '</span></div><div class="timer__caption">Часы</div></div>';
			var Mhtml = '<div class="timer__element"><div class="timer__values"><span class="timer__digit">' + M[0] + '</span><span class="timer__digit">' + M[1] + '</span></div><div class="timer__caption">Минуты</div></div>';
			var Shtml = '<div class="timer__element"><div class="timer__values"><span class="timer__digit">' + S[0] + '</span><span class="timer__digit">' + S[1] + '</span></div><div class="timer__caption">Секунды</div></div>';

			var time = Dhtml + Hhtml + Mhtml + Shtml;
			$this.html(event.strftime(time));
		});
	});
});
// end of timer

function uploadPics(offset) {
	$.ajax({
		dataType: 'json',
		type: 'get',
		url: 'https://www.shense.ru/scripts/pics.php?offset=' + offset,
		success: function(jsondata){
			let html = '';
			let total = 0;
			var re = /([\d\D|\r\n|\r|\n]*)\((.*)\).*/i;
			$.each(jsondata['items'], function(i, data){
				let small_pic = data['photo_604'];
				let big_pic = data['photo_807'];
				let description = data['text'];
				let id = data['id'];
				let title = description.replace(re, "$2");
				let text = description.replace(re, "$1");
				console.log(title);
				html += '<a class="best-works__item" href="#bestWork-' + id +'" data-popup \
						data-effect="mfp-zoom-in"><img src="' + small_pic + '" alt="">\
					    </a><div class="best-works-popup mfp-with-anim mfp-hide" \
					    id="bestWork-' + id + '"><div class="row best-works-popup__row">\
						<div class="col-md-6 best-works-popup__col best-works-popup__col--left">\
						<img class="best-works-popup__img" src="' + big_pic +'" alt="">\
						</div><div class="col-md-6 best-works-popup__col best-works-popup__col--right">\
						<div class="best-works-popup__name">' + title + '</div>\
						<div class="best-works-popup__text scrollable">' + text +'</div>\
						</div></div><div class="best-works-popup__close-back-btn js-close-popup">\
						<i class="fa fa-chevron-left"></i>назад</div></div>';
				total += 1;
			});
			let block_html = $('.best-works').html();
			$('.best-works').html(block_html + html);
			if(total < 8){
				$('#more_pics').css('display', 'none');
			};
		}
	});	
}

$(document).ready(function(){
	var offset = 0;
	uploadPics(offset);

	$('#more_pics').click(function(){
		offset += 8;
		uploadPics(offset);
	});
	// html += uploadPics(offset);
	// await uploadPics(offset);
	// console.log(uploadPics(offset));
});
