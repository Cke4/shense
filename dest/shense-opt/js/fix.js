$( function() {
    var handle = $( "#custom-handle" );
    $( "#slider" ).slider({
      value:250000,
      min: 50000,
      max: 1500000,
      step: 50000,
      create: function() {
        handle.text( '250 000' );
        $('.custom-handle').text('250 000');
      },
      slide: function( event, ui ) {
        handle.text( numberFormat(ui.value.toString(), ' '));
        $('.custom-handle').text(numberFormat(ui.value.toString(), ' '));
        var stores = ui.value / 100000 * 25;
        $('#ct-stores').text(numberFormat(stores.toString(), ' '));
        $('#ct-stores-mobile').text(numberFormat(stores.toString(), ' '));
        if (stores*1830 > 148000)
          {
            var invest = 148000;
          } else {
            var invest = stores * 1830;
          };
        $('#ct-invest').text(numberFormat(invest.toString(), ' '));
        $('#ct-invest-mobile').text(numberFormat(invest.toString(), ' '));


        var cardsales = stores * 7;
        $('#ct-cardsales').text(numberFormat(cardsales.toString(), ' '));
        var toppersales = stores * 3;
        $('#ct-toppersales').text(numberFormat(toppersales.toString(), ' '));
        var pcardsales = stores * 7;
        $('#ct-pcardsales').text(numberFormat(pcardsales.toString(), ' '));
        var boxsales = stores * 1;
        $('#ct-boxsales').text(numberFormat(boxsales.toString(), ' '));

        var cardgross = cardsales * 175;
        $('#ct-cardgross').text(numberFormat(cardgross.toString(), ' '));
        var toppergross = toppersales * 50;
        $('#ct-toppergross').text(numberFormat(toppergross.toString(), ' '));
        var pcardgross = pcardsales * 25;
        $('#ct-pcardgross').text(numberFormat(pcardgross.toString(), ' '));
        var boxgross = boxsales * 250;
        $('#ct-boxgross').text(numberFormat(boxgross.toString(), ' '));

        var cardprofit = cardsales * 85;
        $('#ct-cardprofit').text(numberFormat(cardprofit.toString(), ' '));
        var topperprofit = toppersales * 20;
        $('#ct-topperprofit').text(numberFormat(topperprofit.toString(), ' '));
        var pcardprofit = pcardsales * 15;
        $('#ct-pcardprofit').text(numberFormat(pcardprofit.toString(), ' '));
        var boxprofit = boxsales * 150;
        $('#ct-boxprofit').text(numberFormat(boxprofit.toString(), ' '));

        var totalgross = cardgross + toppergross + pcardgross + boxgross;
        $('#ct-totalgross').text(numberFormat(totalgross.toString(), ' '));
        var totalprofit = cardprofit + topperprofit + pcardprofit + boxprofit;
        $('#ct-totalprofit').text(numberFormat(totalprofit.toString(), ' '));
      }
    });
  } );
  //
  // $( function() {
  //     var handle = $( "#custom-handle-mobile" );
  //     $( "#slider-vertical" ).slider({
  //       orientation: "vertical",
  //       value:250000,
  //       min: 50000,
  //       max: 2500000,
  //       step: 50000,
  //       create: function() {
  //         handle.text( '250 000' );
  //       },
  //       slide: function( event, ui ) {
  //         handle.text( numberFormat(ui.value.toString(), ' '));
  //         var stores = ui.value / 100000 * 30;
  //         $('#ct-stores').text(numberFormat(stores.toString(), ' '));
  //         var invest = stores * 1830;
  //         console.log()
  //         $('#ct-invest').text(numberFormat(invest.toString(), ' '));
  //
  //         var cardsales = stores * 9;
  //         $('#ct-cardsales').text(numberFormat(cardsales.toString(), ' '));
  //         var toppersales = stores * 5;
  //         $('#ct-toppersales').text(numberFormat(toppersales.toString(), ' '));
  //         var pcardsales = stores * 10;
  //         $('#ct-pcardsales').text(numberFormat(pcardsales.toString(), ' '));
  //         var boxsales = stores * 1;
  //         $('#ct-boxsales').text(numberFormat(boxsales.toString(), ' '));
  //
  //         var cardgross = cardsales * 175;
  //         $('#ct-cardgross').text(numberFormat(cardgross.toString(), ' '));
  //         var toppergross = toppersales * 50;
  //         $('#ct-toppergross').text(numberFormat(toppergross.toString(), ' '));
  //         var pcardgross = pcardsales * 25;
  //         $('#ct-pcardgross').text(numberFormat(pcardgross.toString(), ' '));
  //         var boxgross = boxsales * 250;
  //         $('#ct-boxgross').text(numberFormat(boxgross.toString(), ' '));
  //
  //         var cardprofit = cardsales * 85;
  //         $('#ct-cardprofit').text(numberFormat(cardprofit.toString(), ' '));
  //         var topperprofit = toppersales * 20;
  //         $('#ct-topperprofit').text(numberFormat(topperprofit.toString(), ' '));
  //         var pcardprofit = pcardsales * 15;
  //         $('#ct-pcardprofit').text(numberFormat(pcardprofit.toString(), ' '));
  //         var boxprofit = boxsales * 150;
  //         $('#ct-boxprofit').text(numberFormat(boxprofit.toString(), ' '));
  //
  //         var totalgross = cardgross + toppergross + pcardgross + boxgross;
  //         $('#ct-totalgross').text(numberFormat(totalgross.toString(), ' '));
  //         var totalprofit = cardprofit + topperprofit + pcardprofit + boxprofit;
  //         $('#ct-totalprofit').text(numberFormat(totalprofit.toString(), ' '));
  //       }
  //     });
  //   } );


  $( function() {
      $('.calc-tooltip').tooltip();
    } );

  $('.calc-tooltip').on('click', function(){
    $('.ui-tooltip').remove();
  });

// number formatting function 1000000 -> 1 000 000
function numberFormat(_number, _sep) {
_number = Math.round(_number).toString();
_number = typeof _number != "undefined" && _number > 0 ? _number : "";
_number = _number.replace(new RegExp("^(\\d{" + (_number.length%3? _number.length%3:0) + "})(\\d{3})", "g"), "$1 $2").replace(/(\d{3})+?/gi, "$1 ").trim();
if(typeof _sep != "undefined" && _sep != " ") {
    _number = _number.replace(/\s/g, _sep);
}
return _number;}
