$(function() {
	// ----------------------
	// code for popup
	if ($('.best-works').length) {
		$('.best-works').magnificPopup({
			delegate: '.best-works__in-detail a',
			removalDelay: 500,
			callbacks: {
				beforeOpen: function() {
					this.st.mainClass = this.st.el.attr('data-effect');
				}
			}
		});
	}

	initScrollable();

	function initScrollable() {
		if ($(".scrollable").length) {
			$(".scrollable").mCustomScrollbar({
				scrollInertia: 150
			});
		}
	}

	// close popup button
	$(document).on('click', function(e) {
		if ($(e.target).hasClass('js-close-popup')) {
			$.magnificPopup.close();
		}
	});
	// ---------------------/

	// ----------------------
	// scroll to anchor
	$('.js-scroll-to-anchor').click(function(e) {
		e.preventDefault();
		var anchor = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(anchor).offset().top
		}, 600);
	})
	// ---------------------/

	// ---------------------/
	// jquery inview
	$('.js-inview--no-conflict').on('inview', function(event, isInView) {
		if (isInView) {
			$(this).addClass('inview');
		}
	});
	// ---------------------/

	// ---------------------/
	// Basket input
	$('[data-only-numbers]').keydown(function(e) {
		// Allow: backspace, delete, tab, escape, enter
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
			// Allow: Ctrl/cmd+A
			(e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+C
			(e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: Ctrl/cmd+X
			(e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow Ctrl+R
			(e.keyCode == 82 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right
			(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$('.js-basket-item-minus').click(function() {
		var input = $(this).siblings('input');
		var val = input.val();
		var currentValue = val.length ? parseInt(val) : 1;
		if (currentValue > 1) {
			var newValue = currentValue - 1;
			input.val(newValue);
		} else {
			input.val(1);
		}
	});
	$('.js-basket-item-plus').click(function() {
		var input = $(this).siblings('input');
		var val = input.val();
		var currentValue = val.length ? parseInt(val) : 0;
		var newValue = currentValue + 1;
		input.val(newValue);
	});
	// ---------------------/

	// ---------------------/
	// Basket: remove item
	$('.js-remove-basket-item').click(function() {
		$(this).parent().remove();
	});
	// ---------------------/

	// ---------------------
	$('[data-buy-popup]').magnificPopup({
		removalDelay: 500,
		callbacks: {
			beforeOpen: function() {
				this.st.mainClass = this.st.el.attr('data-effect');
			}
		}
	});
	// ---------------------/

	// ---------------------
	// poems
	$('.js-load-new-poems').click(function() {
		$(this).addClass('more-poems__btn--loading');

		setTimeout(function() {
			$('.js-show-after-click-on-load').first().slideDown().removeClass('poems__row--hidden js-show-after-click-on-load');
			if (!$('.js-show-after-click-on-load').length) {
				$(this).fadeOut(500);
			}
		}.bind($(this)), 500);

		setTimeout(function() {
			$(this).removeClass('more-poems__btn--loading');
		}.bind($(this)), 1500);
	});
	// ---------------------/

	// ---------------------
	// feedback
	if ($('.js-init-slider').length) {
		$('.js-init-slider').slick({
			arrows: true,
			fade: true,
			dots: true,
			cssEase: 'linear',
			adaptiveHeight: true
		});
	}
	// ---------------------/

	// ---------------------
	// conditions slider
	if ($('.js-init-conditions-slider').length) {
		$('.js-init-conditions-slider').slick({
			arrows: true,
			fade: true,
			dots: true,
			cssEase: 'linear',
			adaptiveHeight: true
		});
	}
	$('.js-carousel-assortiment').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		autoplay: true,
		autoplaySpeed: 2000,
		slidesToScroll: 1,
		responsive: [
		  {
			breakpoint: 1100,
			settings: {
			  slidesToShow: 3,
			  slidesToScroll: 1
			}
		  },
		  {
			breakpoint: 769,
			settings: {
			  slidesToShow: 2,
			  slidesToScroll: 2
			}
		  },
		  {
			breakpoint: 480,
			settings: {
			  slidesToShow: 1,
			  slidesToScroll: 1
			}
		  }
		  // You can unslick at a given breakpoint now by adding:
		  // settings: "unslick"
		  // instead of a settings object
		]
	  });
	// ---------------------/

	// ---------------------
	// scroll to anchor
	$('.js-scroll-to-anchor').click(function(e) {
		e.preventDefault();

		var href = $(this).attr('href');
		var anchor = $(href);
		if (anchor.length) {
			$('html, body').animate({
				scrollTop: anchor.offset().top
			}, 1000, function() {
				window.location.hash = href;
			});
		}
	})
	// ---------------------/

	$('#submit-info').submit(function(e){
		let errors = 0;
		let inputs = $(this).find('input');
		inputs.each(function(el){
			if($(this).val().length < 1) {
				$(this).addClass('field-empty');
				errors = checkerror(errors, 1);
			}
			else {
				$(this).removeClass('field-empty');
				errors = checkerror(errors, 0);
			}
		});
	    var email = $(this).find('[name="email"]');
	    if(validateEmail(email.val())){
	        email.removeClass('field-empty');
	        errors = checkerror(errors, 0);
	    }
	    else {
	        email.addClass('field-empty');
	        errors = checkerror(errors, 1);
	    }

	    if(errors == 0){
            $.ajax({
                type: "POST",
                url: 'scripts/send_mail.php',
                data: $("#submit-info").serialize(),
                dataType : "json",
                success: function(data)
                {    
   		            console.log(data);
        	        $.magnificPopup.open({
            	        items: {
                	        src: '#sendConfirm' 
                    	},
                    	type: 'inline'
                	});
                },
                error: function(error){
                	console.log(error);
                }
            });	    	
	    }
		e.preventDefault();

	});

	/*MASK FOR PHONE*/
    $("input[name='phone']").mask("+9(999)999-99-99");
});
// contest button
$(function() {
	$('.js-toggle-contest-dropdown').click(function() {
		$('.contest').toggleClass('contest--open');
	});
});
// end of contest button
// timer
$(function() {
	$('[data-countdown]').each(function() {
		var $this = $(this),
			finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
			var D = event.strftime('%D').split('');
			var H = event.strftime('%H').split('');
			var M = event.strftime('%M').split('');
			var S = event.strftime('%S').split('');

			var Dhtml = '<div class="timer__element"><div class="timer__values"><span class="timer__digit">' + D[0] + '</span><span class="timer__digit">' + D[1] + '</span></div><div class="timer__caption">Дни</div></div>';
			var Hhtml = '<div class="timer__element"><div class="timer__values"><span class="timer__digit">' + H[0] + '</span><span class="timer__digit">' + H[1] + '</span></div><div class="timer__caption">Часы</div></div>';
			var Mhtml = '<div class="timer__element"><div class="timer__values"><span class="timer__digit">' + M[0] + '</span><span class="timer__digit">' + M[1] + '</span></div><div class="timer__caption">Минуты</div></div>';
			var Shtml = '<div class="timer__element"><div class="timer__values"><span class="timer__digit">' + S[0] + '</span><span class="timer__digit">' + S[1] + '</span></div><div class="timer__caption">Секунды</div></div>';

			var time = Dhtml + Hhtml + Mhtml + Shtml;
			$this.html(event.strftime(time));
		});
	});
});
// end of timer

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function checkerror(errors, error) {
	let final_errors = 0;
	if(errors == 1){
		final_errors = 1;
	}
	if(error == 1){
		final_errors = 1;
	}
	return final_errors;
}