$(function() {
    //js for catalog cards
    $.each($('.product__list'), function(){
      if ($(this).children().length < 4) {
              $(this).addClass('product__list-one-child');
    
            };
    
    });

    // filter catalog
    $('.js-catalog-filter a').click(function(e){
        let element_click = $(this).attr('href');
        $('.catalog__item').css('display', 'none');
        switch (element_click) {
            case '#other':
                $('div.catalog__item:not(#toppery_shense):not(#derevyannye_korobochki_shense):not(#birochki_shense):not(#catalog-events)').css('display', 'block');
                $('#catalog-events > .catalog__item').css('display', 'none');
            case '#catalog-events':
                $(element_click).css('display', 'block');
                $(element_click + ' .catalog__item').css('display', 'block');
            default:
                $(element_click).css('display', 'block');
        }
        e.preventDefault();
    });


    $('a.js-catalog_clear-filter').click(function(e){
        $('.catalog__item').css('display', 'block');
        e.preventDefault();
    });


    $('.js-toggle-collection-new').click(function(e){
        $(this).hide();
        var block = $(this).parent().next();
        if(block.length){
            console.log(block.length);
            block.addClass("shown-all-items");
            // $BODY.scrollTo(e, {
            //     duration: 200,
            //     offset: -HEADER_HEIGHT
            // });
        }
        e.preventDefault();
    });


    $('.js-toggle-collection-new-mobile').click(function(e){
        $(this).hide();
        var block = $(this).prev();
        if(block.length){
            console.log(block.length);
            block.addClass("shown-all-items");
            // $BODY.scrollTo(e, {
            //     duration: 200,
            //     offset: -HEADER_HEIGHT
            // });
        }
        e.preventDefault();
    });
    // submit contacts form
    $('.js-submit-contacts-form').on('submit', function(e) {
        var errors = 0;
        $(this).find('[required]').each(function() {
            var val = $(this).val().trim();
            if (!val.length) {
                $(this).parent().addClass('error');
                errors++;
            } else {
                $(this).parent().removeClass('error');
            }
        });

        if (errors) {
            e.preventDefault();
        }
    });

    $('input, textarea').on('keyup keypress', function() {
        $(this).parent().removeClass('error');
    });
    // submit contacts form --/    
    // ----------------------
    // code for popup
    if ($('.best-works').length) {
        $('.best-works').magnificPopup({
            delegate: '.best-works__in-detail a',
            removalDelay: 500,
            callbacks: {
                beforeOpen: function() {
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
            }
        });
    }

    initScrollable();

    function initScrollable() {
        if ($(".scrollable").length) {
            $(".scrollable").mCustomScrollbar({
                scrollInertia: 150
            });
        }
    }

    // close popup button
    $(document).on('click', function(e) {
        if ($(e.target).hasClass('js-close-popup')) {
            $.magnificPopup.close();
        }
    });
    // ---------------------/

    // ----------------------
    // scroll to anchor
    $('.js-scroll-to-anchor').click(function(e) {
        e.preventDefault();
        var anchor = $(this).attr('href');
        $('html, body').animate({
            scrollTop: $(anchor).offset().top
        }, 600);
    })
    // ---------------------/

    // ---------------------/
    // jquery inview
    $('.js-inview--no-conflict').on('inview', function(event, isInView) {
        if (isInView) {
            $(this).addClass('inview');
        }
    });
    // ---------------------/      
    // ---------------------/
    // Basket input
    $('[data-only-numbers]').keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow Ctrl+R
            (e.keyCode == 82 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    // ---------------------/

    // ---------------------
    $('[data-buy-popup]').magnificPopup({
        removalDelay: 500,
        callbacks: {
            beforeOpen: function() {
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        }
    });
    // ---------------------/

    // toggle new menu
    var isAllowedToToggleNewmenu = true;
    $('.js-toggle-newmenu').click(function(e) {
        e.preventDefault();

        if (isAllowedToToggleNewmenu) {
            isAllowedToToggleNewmenu = false;

            $(this).toggleClass('open');

            $('#newmenu').fadeToggle(200, function() {
                isAllowedToToggleNewmenu = true;
                if ($('#newmenu').is(':visible')) {
                    $('#newmenu .newmenu-list-wrap').hide().first().slideDown(100);
                    $('header').addClass('static');
                    $('html, body').scrollTop(0);
                    $('.newnav .newnav__item').removeClass('active').first().addClass('active');
                } else {
                    closeNewMenu();
                }
            });
        }
    });

    function closeNewMenu() {
        $('#newmenu').hide();
        $('#newmenu .newmenu-list-wrap').hide();
        $('header').removeClass('static');
        $('.js-toggle-newmenu').removeClass('open');
        $('.newnav__item').removeClass('active');
    }

    $(document).mouseup(function(e) {
        if (!$('#newmenu').is(e.target) && $(e.target).parents('#newmenu').length === 0 && !$(e.target).is('.js-toggle-newmenu') && $(e.target).parents('.js-toggle-newmenu').length === 0) {
            closeNewMenu();
        }
    });

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $('.js-open-sub-newmenu').click(function(e) {
        $('.newnav__item').removeClass('active');
        $(this).parents('.newnav__item').addClass('active');

        var submenu = $(this).data('target');
        $('.newmenu-list-wrap:not(' + submenu + ')').hide();
        $(submenu).fadeIn(200);
    });
} else {
    $('.js-open-sub-newmenu').hover(function(e) {
        $('.newnav__item').removeClass('active');
        $(this).parents('.newnav__item').addClass('active');

        var submenu = $(this).data('target');
        console.log(111);
        $('.newmenu-list-wrap').hide();
        $('.newmenu-list-wrap' + submenu).show();
        $(submenu).fadeIn(200);
    });
}
    // toggle new menu --/

    window.supports_html5_storage = function() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    };
    window.getCart = function(){
        if (!supports_html5_storage()) {
            return;
        }
        return JSON.parse(localStorage['cart'] || '{}') ;
    }

    window.setCart = function(cart){
        if (!supports_html5_storage()) {
            return;
        }
        cart = cart || {};
        localStorage['cart'] = JSON.stringify(cart);
    }

    window.addToCart = function(article, amount, addOrSet) {
        amount = amount === 0 ? amount : amount||1;
        addOrSet = addOrSet || 0;
        if (!supports_html5_storage()) {
            return;
        }

        var cart = getCart();
        cart[article] = cart[article] || {amount:0};

        if (addOrSet === 0) {
            cart[article]['amount'] = (cart[article]['amount'] || 0) + amount;
        }
        else {
            cart[article]['amount'] = amount;
        }   
        if(cart[article]['amount'] > 0 ){
          cart[article].article = article;
          cart[article].category = $('span.product-card-caption--category').text();
          cart[article].title = $('span.product-card-caption--name').text();
          cart[article].size = $('dd.param--value[param="Размер"]').text(); 
          cart[article].price = $('div.product-card--price').clone().children().remove().end().text().replace(/\r?\n|\r/g,"").trim();
          cart[article].pic = $('div.tos-slide:first-of-type img').attr('src');
        } else {
          delete cart[article];
        }
        setCart(cart);
    };

    window.removeFromCart = function(article, type) {
        addToCart(article,0,1, type);
    }
    window.clearCart = function(){console.log(1);setCart({}); };

    $('body').on('click','.product-btn.product-btn--in-basket', function(){
        window.location = '/basket/basket.html';
    });

    /* ADD TO CART */
    $('body').on('click','.product-btn:not(.product-btn--in-basket,.product-btn--mini)', function(){
        article = $('dd.param--value[param="Артикул"]').text(); 
        addToCart(article, 1);
        $('.js-basket-add').addClass('product-btn--in-basket');
        showCartHeader();
    });

    /* REMOVE FROM CART */
    $('body').on('click','.js-remove-basket-item', function() {
        var article = $(this).data('article');
        removeFromCart(article);
        $(this).parent().remove();
        showCart();
        showCartHeader();
        showNotification();
    });

    /*PLUS AMOUNT IN CART*/
    $('body').on('click','.js-basket-item-plus', function() {
        var input = $(this).siblings('input');
        var val = input.val();
        var currentValue = val.length ? parseInt(val) : 0;
        var newValue = currentValue + 1;
        input.val(newValue);
        var article = $(this).closest('.basket-list__row').prev().data('article');
        var cart = getCart();
        var amount = cart[article]['amount'];
        cart[article]['amount'] = amount + 1;
        setCart(cart);
        showCart();
        showCartHeader();  
    });

    /*MINUS ITEM IN CART*/
    $('body').on('click','.js-basket-item-minus', function() {
        var input = $(this).siblings('input');
        var val = input.val();
        var currentValue = val.length ? parseInt(val) : 1;
        if (currentValue > 1) {
            var newValue = currentValue - 1;
            input.val(newValue);
        } else {
            input.val(1);
        }
        var article = $(this).closest('.basket-list__row').prev().data('article');
        var cart = getCart();
        var amount = cart[article]['amount'];
        cart[article]['amount'] = amount - 1;
        if(cart[article]['amount'] == 0){
            cart[article]['amount'] = 1;
        }
        setCart(cart);
        showCart();
        showCartHeader(); 
    });

    /*MASK FOR PHONE*/
    $("input[name='phone']").mask("+9(999)999-99-99");

    /* ONE CLICK */
    $('[href="#buyOneClick"]').click(function(){
        clearCart();
        showCartHeader();
        $('.product-btn').removeClass('product-btn--in-basket');
    });

    $('#cartOrderOclck').submit(function(e) {
        clearCart();
        var url = "https://shense.ru/scripts/cart.php";
        article = $('dd.param--value[param="Артикул"]').text(); 
        addToCart(article, 1);
        $('#cartOrderOclckInputOrder').val(localStorage['cart']);
        var check = checkOrderForm('#cartOrderOclck');
        if(check == false){
            return false;
        }

        $.ajax({
               type: "POST",
               url: url,
               data: $("#cartOrderOclck").serialize(),
               dataType : "json",
               success: function(data)
               {    
                    console.log(data);
                    $('#CustomerNumber').val(data['CustomerNumber']);
                    $('#Sum').val(data['Sum']);
                    $('#CustName').val(data['CustName']);
                    $('#CustAddr').val(data['CustAddr']);
                    $('#CustEMail').val(data['CustEMail']);
                    $('#cps_email').val(data['cps_email']);
                    $('#cps_phone').val(data['cps_phone']);
                    $('#CustomerNumber').val(data['CustomerNumber']);
                    $('#orderDetails').val(data['orderDetails']);
                    // $('#orderConfirm').css('display', 'block');

                    $.magnificPopup.open({
                        items: {
                            src: '#orderConfirm' 
                        },
                        type: 'inline'
                    });
                    
               }
             });

        e.preventDefault();
    });
    showCartHeader();


    /* CLEAR CART */
    $('.yandex-button').click(function(){
        clearCart();
    });  


});

/* SHOW CART */
var showCart = function(){
    var cart = getCart();
    if(!$.isEmptyObject(cart)){
        var cartItems = JSON.parse(localStorage['cart']); 
        var html = '';
        var total_sum = 0;
        for (i in cartItems){
            // console.log(cartItems[i].price);
            if(cartItems[i].amount > 0){
                var total_amount = cartItems[i].price * cartItems[i].amount;
                html += '<li class="basket-list__item">\
                        <button class="basket-list__close js-remove-basket-item" data-article="' + cartItems[i].article + '"></button>\
                        <div class="row basket-list__row">\
                            <div class="col-sm-7 col-xs-9 basket-list__col basket-list__col--1">\
                                <div class="basket-list__col-inner">\
                                    <div class="basket-list__img-wr">\
                                        <img src="'  + cartItems[i].pic + '" alt="">\
                                    </div>\
                                    <div class="basket-list__info">\
                                        <div class="basket-list__item-name">' + cartItems[i].category + ' ' + cartItems[i].title + '</div>\
                                        <div class="basket-list__item-article">Артикул: ' + cartItems[i].article + '\
                                            <br>' + cartItems[i].size + '</div>\
                                    </div>\
                                </div>\
                            </div>\
                            <div class="col-sm-3 col-xs-3 basket-list__col basket-list__col--2">\
                                <div class="basket-list__col-inner basket-list__col-inner--counter">\
                                    <div class="basket-counter">\
                                        <button class="basket-counter__btn basket-counter__btn--minus js-basket-item-minus"></button>\
                                        <input class="basket-counter__field" type="text" value="' + cartItems[i].amount + '" data-only-numbers disabled>\
                                        <button class="basket-counter__btn basket-counter__btn--plus js-basket-item-plus"></button>\
                                    </div>\
                                </div>\
                            </div>\
                            <div class="col-sm-2 col-xs-12 basket-list__col basket-list__col--3">\
                                <div class="basket-list__col-inner basket-list__col-inner--price">\
                                    <div class="basket-list__price">\
                                        <span class="basket-list__price-value">' + total_amount + '</span>\
                                        <span class="basket-list__price-currency">рублей</span>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </li>';
                total_sum += total_amount;
            }
        }
        // console.log(html);
        $('.basket-list').html(html);
        if(total_sum < 349) {
            total_sum += 100;
            $('.shipping__delivery-free').addClass('non-shown');
            $('.basket-shipping__value').removeClass('non-shown');
        } else {
            $('.shipping__delivery-free').removeClass('non-shown');
            $('.basket-shipping__value').addClass('non-shown');
        }
        $('.basket-total__value').text(total_sum);
        // $('#cart-table .format').each(function(){
          // $(this).text(formatPrice($(this).text()));
        // });    
    }
}

var showCartHeader = function(){
    var cart = getCart();
    if(!$.isEmptyObject(cart)){
        var cartItems = JSON.parse(localStorage['cart']); 
        var html = '';
        var total_sum = 0;
        for (i in cartItems){
            if(cartItems[i].amount > 0){
                var total_amount = cartItems[i].price * cartItems[i].amount;
                total_sum += total_amount;
            }
        }
        $('.header--basket-link').addClass('full');
        $('.header--basket-link span').text(total_sum + ' руб');
    } else {
        $('.header--basket-link').removeClass('full');
    }
}

var checkOrderForm = function(e){
    var error = 0;
    var zipcode = $(e).find('[name="zipcode"]');
    if(zipcode.val().length < 6){
        zipcode.addClass('form-field--empty');
        error = 1;
    }
    else {
        zipcode.removeClass('form-field--empty');
        error = 0;
    }

    var address = $(e).find('[name="address"]');
    if(address.val().length < 5){
        address.addClass('form-field--empty');
        error = 1;
    }
    else {
        address.removeClass('form-field--empty');
        error = checkOrderFormError(error);
    }

    var name = $(e).find('[name="name"]');
    if(name.val().length < 1){
        name.addClass('form-field--empty');
        error = 1;
    }
    else {
        name.removeClass('form-field--empty');
        error = checkOrderFormError(error);
    }

    var phone = $(e).find('[name="phone"]');
    if(phone.val().length < 16){
        phone.addClass('form-field--empty');
        error = 1;
    }
    else {
        phone.removeClass('form-field--empty');
        error = checkOrderFormError(error);
    }

    var email = $(e).find('[name="email"]');
    if(validateEmail(email.val())){
        email.removeClass('form-field--empty');
        error = checkOrderFormError(error);
    }
    else {
        email.addClass('form-field--empty');
        error = 1;
    }

    if(error == 1){
        return false;
    }
}

var showNotification = function(){
    var cart = getCart();
    if($.isEmptyObject(cart)){
        $('.basket-page .basket-container').addClass('non-shown');
        $('.basket-page .notification').removeClass('non-shown');
    };
}

function checkOrderFormError(e){
    if(e == 1){
        return 1;
    }
    else {
        return 0;
    }
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}